export default class AddFile {
  constructor() {
    this.form = document.querySelector('[data-id=form]');
    this.traffic = document.querySelector('[data-id=traffic]');
    this.fileEl = document.querySelector('[data-id=file]');
    this.overlapEl = document.querySelector('[data-id=overlap]');
    this.fileContainer = document.querySelector('[data-id=file-container]');

    this.idCount = 0;
    this.files = [];
  }

  create() {
    this.addListenerOverlapEl();
    this.addListenerFileEl();
    this.addListenerForm();
  }

  downloadFile(obj) {
    const file = this.files[obj];
    const a = document.createElement('a');
    a.download = file.name;
    a.href = URL.createObjectURL(file);
    a.rel = 'noopener';
    setTimeout(() => URL.revokeObjectURL(a.href), 60000);
    setTimeout(() => a.dispatchEvent(new MouseEvent('click')));

    const downloadFileis = document.getElementById(obj);
    const downloadFileSize = Number(downloadFileis.children[1].textContent.substr(0, 3));
    this.traffic.textContent = (Number(this.traffic.textContent) + downloadFileSize).toFixed(2);
  }

  sizeConverter(weight) {
    const size = weight / 1024 / 1024;
    return `${size.toFixed(1)} Mb`;
  }

  addFiles(files) {
    for (let i = 0; i < files.length; i += 1) {
      const divEl = document.createElement('div');
      divEl.className = 'file';
      divEl.id = this.idCount;

      divEl.innerHTML = `
        <span class="name">${files[i].name}</span>
        <span class="size">${this.sizeConverter(files[i].size)}</span>
        <span class="link">Download</span>`;

      this.fileContainer.appendChild(divEl);
      this.fileEl.value = '';
      this.idCount += 1;
    }
  }

  // события для запуска окна выбора файлов
  addListenerOverlapEl() {
    this.overlapEl.addEventListener('click', () => {
      this.fileEl.dispatchEvent(new MouseEvent('click'));
    });
  }

  addListenerFileEl() {
    this.fileEl.addEventListener('change', (evt) => {
      this.files = Array.from(evt.currentTarget.files);
      this.overlapEl.style.display = 'none';

      this.addFiles(this.files);
    });
  }

  addListenerForm() {
    this.form.addEventListener('click', (evt) => {
      if (evt.toElement.className === 'link') {
        const obj = evt.toElement.parentNode.id;
        this.downloadFile(obj);
      }
    });
  }
}
